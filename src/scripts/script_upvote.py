from src.utils.browser import Browser
import time
from selenium.webdriver.common.by import By
from src.utils.kameleo import Kameleo
from src.utils.browser import Browser
import threading
import random
import math
import logging
from datetime import datetime
import json


class ScriptUpvote:
    def __init__(self, reddit_post_url, upvotes_number, thread_number):
        logging.basicConfig(filename='log.txt',
                            filemode='w',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=logging.INFO)
        self.logger = logging.getLogger('reddit_upvoter')
        self.profiles = Kameleo().get_all_profiles()
        self.reddit_post_url = reddit_post_url
        self.upvotes_number = upvotes_number
        self.upvotes_done = 0
        # .replace('www.reddit.com', 'old.reddit.com')
        self.subreddit_url = self.reddit_post_url.split('comments')[0]
        self.threads_num = thread_number

    def start(self):
        for i in range(self.threads_num):
            thread = threading.Thread(name=f'thread{i}', target=self.have_fun)
            thread.start()

    def log(self, info):
        self.logger.info(info)
        print(info)

    def have_fun(self):
        def is_banned():
            browser.open('https://www.reddit.com/appeals', sleep=1)
            if not browser.element_exists('//*[contains(text(), "cannot submit an appeal")]', by=By.XPATH):
                browser.refresh(sleep=1)
            ban1 = not browser.element_exists('//*[contains(text(), "cannot submit an appeal")]', by=By.XPATH)
            ban2 = browser.element_exists('//*[contains(text(), "account has been suspended")]', by=By.XPATH)
            return ban1 or ban2

        def is_aged():
            browser.open('https://www.reddit.com/user/me/about.json')
            created_timestamp = int(browser.browser.page_source.split('"created": ')[-1].split('.')[0])
            created_date = datetime.fromtimestamp(created_timestamp)
            timedelta = datetime.now() - created_date
            return timedelta.days > 6

        def unstack():
            # close popup
            browser.click('//button[@aria-label="Close"]', by=By.XPATH)
            # accept 18+
            if browser.element_exists('//*[contains(text(), "You must be 18+")]', by=By.XPATH):
                browser.click('//button[contains(text(), "Yes")]', by=By.XPATH)

            if browser.element_exists('//*[contains(text(), "couldn\'t load posts")]', by=By.XPATH):
                browser.refresh()

            if browser.element_exists('//img[@alt="you broke reddit"]', by=By.XPATH):
                browser.refresh()

        def fake_subreddit_surf():
            browser.open(self.subreddit_url)
            browser.random_scroll()

        def upvote() -> bool:
            # upvote_button = browser.find_element('//button[@data-click-id="upvote"]', by=By.XPATH)
            upvote_button = browser.find_element(
                '//button[@data-click-id="upvote"]', by=By.XPATH)
            if upvote_button:
                upvoted = 'true' in upvote_button.get_attribute('aria-pressed')
                if not upvoted:
                    upvote_button.click()
                    time.sleep(random.randint(7, 10))
                    upvoted = 'true' in upvote_button.get_attribute('aria-pressed')
                return upvoted
            else:
                return False

        def print_stats():
            upvoted = 0
            done = 0
            error = 0
            for profile in self.profiles:
                if profile.done:
                    done += 1
                    if profile.upvoted:
                        upvoted += 1
                    else:
                        error += 1
            self.log(
                f"STATS:"
                f"\n{done}/{len(self.profiles)} profiles worked;"
                f"\n{upvoted}/{done} profiles successfully upvoted;"
                f"\n{error}/{done} profiles failed upvote;"
                f"\n{upvoted}/{self.upvotes_number} upvotes competed.")

        thread_num = int(threading.current_thread().name.replace('thread', ''))
        profiles_per_thread = int(
            math.ceil(len(self.profiles) / self.threads_num))
        start_index = profiles_per_thread * thread_num
        end_index = start_index + profiles_per_thread if len(
            self.profiles) > start_index + profiles_per_thread else len(self.profiles)
        thread_profiles = self.profiles[start_index:end_index]

        while len(thread_profiles) > 0 and self.upvotes_done < self.upvotes_number:
            browser = None
            idx = random.randint(0, len(thread_profiles)-1)
            profile = thread_profiles.pop(idx)
            if not profile.done:
                try:
                    idx += start_index
                    self.log(f"Thread {thread_num}: takes profile {idx} ({profile.name})")
                    browser = Browser(profile)

                    # if not is_aged():
                    #     self.log(f"Thread {thread_num}: profile {idx} ({profile.name}) is <6 days old =(")
                    # elif is_banned():
                    #     profile.banned = True
                    #     self.log(f"Thread {thread_num}: profile {idx} ({profile.name}) banned =(")
                    fake_subreddit_surf()
                    browser.open(self.reddit_post_url)
                    upvoted = upvote()
                    i = 0
                    while not upvoted and i < 5:
                        unstack()
                        upvoted = upvote()
                        i += 1

                    if upvoted:
                        profile.upvoted = True
                        self.upvotes_done += 1
                        self.log(
                            f"Thread {thread_num}: profile {idx} ({profile.name}) upvoted successfully")
                        browser.random_scroll()
                    else:
                        self.log(
                            f"Thread {thread_num}: profile {idx} ({profile.name}) upvote FAILED!!!")

                    profile.done = True
                    browser.exit()
                    print_stats()

                except Exception as e:
                    self.log(
                        f"Thread {thread_num} ERROR: profile {idx} - {e}")
                    if profile:
                        profile.done = True
                    if browser:
                        browser.exit()
                    pass
