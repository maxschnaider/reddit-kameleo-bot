from src.utils.browser import Browser
import time
from selenium.webdriver.common.by import By
from src.utils.kameleo import Kameleo
from src.utils.browser import Browser
import threading
import random
import math
import logging
from datetime import datetime
import json


class ScriptBan:
    def __init__(self, thread_number):
        logging.basicConfig(filename='log.txt',
                            filemode='w',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=logging.INFO)
        self.logger = logging.getLogger('reddit_upvoter')
        self.profiles = Kameleo().get_all_profiles(only_aged=False)
        self.threads_num = thread_number

    def start(self):
        for i in range(self.threads_num):
            thread = threading.Thread(name=f'thread{i}', target=self.have_fun)
            thread.start()

    def log(self, info):
        self.logger.info(info)
        print(info)

    def have_fun(self):
        def is_banned():
            browser.open('https://www.reddit.com/appeals', sleep=1)
            if not browser.element_exists('//*[contains(text(), "cannot submit an appeal")]', by=By.XPATH):
                browser.refresh(sleep=1)
            ban1 = not browser.element_exists('//*[contains(text(), "cannot submit an appeal")]', by=By.XPATH)
            ban2 = browser.element_exists('//*[contains(text(), "account has been suspended")]', by=By.XPATH)
            return ban1 or ban2

        def print_stats():
            done = 0
            banned = 0
            for profile in self.profiles:
                if profile.done:
                    done += 1
                    if profile.banned:
                        banned += 1
            self.log(
                f"STATS:"
                f"\n{done}/{len(self.profiles)} profiles checked;"
                f"\n{banned}/{done} profiles banned;")

        thread_num = int(threading.current_thread().name.replace('thread', ''))
        profiles_per_thread = int(
            math.ceil(len(self.profiles) / self.threads_num))
        start_index = profiles_per_thread * thread_num
        end_index = start_index + profiles_per_thread if len(
            self.profiles) > start_index + profiles_per_thread else len(self.profiles)
        thread_profiles = self.profiles[start_index:end_index]

        while len(thread_profiles) > 0:
            browser = None
            idx = random.randint(0, len(thread_profiles)-1)
            profile = thread_profiles.pop(idx)
            if not profile.done:
                try:
                    idx += start_index
                    self.log(f"Thread {thread_num}: takes profile {idx} ({profile.name})")
                    browser = Browser(profile)

                    if is_banned():
                        profile.banned = True
                        self.log(f"Thread {thread_num}: profile {idx} ({profile.name}) banned =(")

                    profile.done = True
                    browser.exit()
                    print_stats()

                except Exception as e:
                    self.log(
                        f"Thread {thread_num} ERROR: profile {idx} - {e}")
                    if profile:
                        profile.done = True
                    if browser:
                        browser.exit()
                    pass
